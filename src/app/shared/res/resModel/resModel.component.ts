import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ResModel } from '../../../services/api/models/resModel.model';
import { ResBrandsClient } from '../../../services/api/resBrand.service';
import { WindowService } from '../../../services/window.service';

@Component({
  selector: 'app-res-model',
  templateUrl: './resModel.component.html',
  styleUrls: ['./resModel.component.scss']
})
export class ResModelComponent {
  public isBusy = false;
  @Input() type = 'newModel';
  @Input() brandId = 0;
  @Input() modelHeli: ResModel;
  @Output() ngOnError: EventEmitter<string> = new EventEmitter<string>();
  @Output() ngOnSuccess: EventEmitter<string> = new EventEmitter<string>();

  constructor(private client: ResBrandsClient, private mdserv: WindowService) {
    this.modelHeli = new ResModel();
    this.modelHeli.id = 0;
    this.modelHeli.resBrandId = this.brandId;
  }
  get saveDisabled() { return this.modelHeli.name == null || this.modelHeli.name.length === 0; }

  editModel() {
    this.isBusy = true;
    this.client.updateResModel(this.modelHeli).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('update success');
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Сохранение модели квадрокоптера', error.status);
      this.ngOnError.emit('update error ' + error.status);
    });
  }
  createModel() {
    this.isBusy = true;
    this.modelHeli.id = 0;
    this.modelHeli.resBrandId = this.brandId;
    this.client.createResModel(this.modelHeli).subscribe(x => {
      this.isBusy = false;
      this.modelHeli = new ResModel();
      this.modelHeli.id = 0;
      this.modelHeli.resBrandId = this.brandId;
      this.ngOnSuccess.emit('creation success');
    }, error => {
      this.isBusy = false;
      this.modelHeli = new ResModel();
      this.modelHeli.id = 0;
      this.modelHeli.resBrandId = this.brandId;
      this.mdserv.showServerError('Создание модели квадрокоптера', error.status);
      this.ngOnError.emit('creation error ' + error.status);
    });
  }
  deleteModel() {
    this.isBusy = true;
    this.client.deleteResModel(this.modelHeli.id).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('deletion success');
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Удаление модели квадрокоптера', error.status);
      this.ngOnError.emit('deletion error ' + error.status);
    });
  }

  showWindow() {
    this.mdserv.showDeleteMessage('Удаление модели', 'Вы действительно хотите удалить модель: ' +
      this.modelHeli.name + '?').subscribe(x => {
        if (x) { this.deleteModel(); }
      });
  }



}
