import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ResBrand } from '../../../services/api/models/resBrand.model';
import { ResBrandsClient } from '../../../services/api/resBrand.service';
import { WindowService } from '../../../services/window.service';

@Component({
  selector: 'app-res-brand',
  templateUrl: './resBrand.component.html',
  styleUrls: ['./resBrand.component.scss']
})
export class ResBrandComponent {
  public isBusy = false;
  public mode: 'show' | 'edit' = 'show';

  @Input() type = 'newBrand';
  @Input() selected = false;
  @Input() brand: ResBrand;

  get editIcon() { return this.mode === 'show' ? 'fa-edit' : 'fa-save'; }
  get deleteIcon() { return this.mode === 'show' ? 'fa-trash' : 'fa-times'; }
  get saveDisabled() { return this.brand.name == null || this.brand.name.length === 0; }

  @Output() ngOnError: EventEmitter<string> = new EventEmitter<string>();
  @Output() ngOnSuccess: EventEmitter<string> = new EventEmitter<string>();
  @Output() ngOnBrandSelect: EventEmitter<ResBrand> = new EventEmitter<ResBrand>();

  constructor(private client: ResBrandsClient, private mdserv: WindowService) {
    this.brand = new ResBrand();
    this.brand.id = 0;
  }

  selectResBrand() { this.ngOnBrandSelect.emit(this.brand); }

  pushFirstButton() {
    if (this.mode === 'show') {
      this.mode = 'edit';
    } else {
      this.editResBrand();
    }
  }
  pushSecondButton() {
    if (this.mode === 'show') {
      this.showWindow();
    } else {
      this.mode = 'show';
      this.ngOnSuccess.emit('no edditing');
    }
  }

  editResBrand() {
    this.isBusy = true;
    this.client.updateResBrand(this.brand).subscribe(x => {
      this.isBusy = false;
      this.mode = 'show';
      this.ngOnSuccess.emit('update success');
    }, error => {
      this.isBusy = false;
      this.mode = 'show';
      this.mdserv.showServerError('Сохранение марки квадрокоптера', error.status);
      this.ngOnError.emit('update error ' + error.status);
    });
  }
  createResBrand() {
    this.isBusy = true;
    this.client.createResBrand(this.brand).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('creation success');
      this.brand = new ResBrand();
      this.brand.id = 0;
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Создание марки квадрокоптера', error.status);
      this.ngOnError.emit('creation error ' + error.status);
    });
  }
  deleteResBrand() {
    this.isBusy = true;
    this.client.deleteResBrand(this.brand.id).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('deletion success ' + this.brand.id);
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Удаление марки квадрокоптера', error.status);
      this.ngOnError.emit('deletion error ' + error.status);
    });
  }

  showWindow() {
    this.mdserv.showDeleteMessage('Удаление марки', ''
      + 'Вы действительно хотите удалить марку: ' + this.brand.name + '?').subscribe(x => {
        if (x) { this.deleteResBrand(); }
      });
  }
}
