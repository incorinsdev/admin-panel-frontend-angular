import { Component } from '@angular/core';
import { ResBrand } from '../../../services/api/models/resBrand.model';
import { ResBrandsClient } from '../../../services/api/resBrand.service';
import { WindowService } from '../../../services/window.service';

@Component({
  selector: 'app-res-brand-controller',
  templateUrl: './resBrand-controller.component.html',
  styleUrls: ['./resBrand-controller.component.scss']
})
export class ResBrandControllerComponent {
  brandArray: Array<ResBrand>;
  selectedBrand: ResBrand;
  constructor(private client: ResBrandsClient, private mdserv: WindowService) {
    this.brandArray = [];
    this.updateBrands();
  }


  updateBrands() {
    this.client.getResBrands().subscribe(data => {
      this.brandArray = data.sort((a, b) => a.id > b.id ? 1 : -1);
      this.selectedBrand = this.selectedBrand ? this.brandArray.find(x => x.id === this.selectedBrand.id) : null;
    }, error => {
      this.mdserv.showServerError('Загрузка марок квадрокоптеров', error.status);
    });
  }

  updateOnSucces(event: string) { this.updateBrands(); }
  updateOnError(event: string) { this.updateBrands(); }
}
