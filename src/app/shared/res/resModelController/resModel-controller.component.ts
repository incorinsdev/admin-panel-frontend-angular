import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ResBrand } from '../../../services/api/models/resBrand.model';

@Component({
  selector: 'app-res-model-controller',
  templateUrl: './resModel-controller.component.html'
})
export class ResModelControllerComponent {
  @Input() brand: ResBrand;

  get modelArray() { return []; }

  @Output() ngNeedUpdate: EventEmitter<string> = new EventEmitter<string>();

  updateOnSucces(event: string) {
    this.ngNeedUpdate.emit(event);
  }
  updateOnError(event: string) {
    this.ngNeedUpdate.emit(event);
  }
}
