import { Component, Input, EventEmitter, Output } from '@angular/core';
import { InsBrand } from '../../../services/api/models/insBrand.model';
import { InsBrandsClient } from '../../../services/api/insBrand.service';
import { WindowService } from '../../../services/window.service';

@Component({
  selector: 'app-ins-brand',
  templateUrl: './insBrand.component.html',
  styleUrls: ['./insBrand.component.scss']
})
export class InsBrandComponent {

  public isBusy = false;
  public mode: 'show' | 'edit' = 'show';

  @Input() type = 'newBrand';
  @Input() selected = false;
  @Input() brand: InsBrand;

  get editIcon() { return this.mode === 'show' ? 'fa-edit' : 'fa-save'; }
  get deleteIcon() { return this.mode === 'show' ? 'fa-trash' : 'fa-times'; }
  get saveDisabled() { return this.brand.name == null || this.brand.name.length === 0; }

  @Output() ngOnError: EventEmitter<string> = new EventEmitter<string>();
  @Output() ngOnSuccess: EventEmitter<string> = new EventEmitter<string>();
  @Output() ngOnBrandSelect: EventEmitter<InsBrand> = new EventEmitter<InsBrand>();

  constructor(private client: InsBrandsClient, private mdserv: WindowService) {
    this.brand = new InsBrand();
    this.brand.id = 0;
  }

  selectInsBrand() { this.ngOnBrandSelect.emit(this.brand); }

  pushFirstButton() {
    if (this.mode === 'show') {
      this.mode = 'edit';
    } else {
      this.editInsBrand();
    }
  }
  pushSecondButton() {
    if (this.mode === 'show') {
      this.showWindow();
    } else {
      this.mode = 'show';
      this.ngOnSuccess.emit('no edditing');
    }
  }

  editInsBrand() {
    this.isBusy = true;
    this.client.updateInsBrand(this.brand).subscribe(x => {
      this.isBusy = false;
      this.mode = 'show';
      this.ngOnSuccess.emit('update success');
    }, error => {
      this.isBusy = false;
      this.mode = 'show';
      this.mdserv.showServerError('Сохранение марки квадрокоптера', error.status);
      this.ngOnError.emit('update error ' + error.status);
    });
  }
  createInsBrand() {
    this.isBusy = true;
    this.client.createInsBrand(this.brand).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('creation success');
      this.brand = new InsBrand();
      this.brand.id = 0;
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Создание марки квадрокоптера', error.status);
      this.ngOnError.emit('creation error ' + error.status);
    });
  }
  deleteInsBrand() {
    this.isBusy = true;
    this.client.deleteInsBrand(this.brand.id).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('deletion success ' + this.brand.id);
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Удаление марки квадрокоптера', error.status);
      this.ngOnError.emit('deletion error ' + error.status);
    });
  }

  showWindow() {
    this.mdserv.showDeleteMessage('Удаление марки', 'При удалении марки уничтожаться также и все ее модели квадрокоптеров.'
      + 'Вы действительно хотите удалить марку: ' + this.brand.name + '?').subscribe(x => {
        if (x) { this.deleteInsBrand(); }
      });
  }

}
