import { Component, Input, Output, EventEmitter } from '@angular/core';
import { InsModel } from '../../../services/api/models/insModel.model';
import { InsBrandsClient } from '../../../services/api/insBrand.service';
import { WindowService } from '../../../services/window.service';

@Component({
  selector: 'app-ins-model',
  templateUrl: './insModel.component.html',
  styleUrls: ['./insModel.component.scss']
})
export class InsModelComponent {

  public isBusy = false;
  @Input() type = 'newModel';
  @Input() brandId = 0;
  @Input() modelHeli: InsModel;
  @Output() ngOnError: EventEmitter<string> = new EventEmitter<string>();
  @Output() ngOnSuccess: EventEmitter<string> = new EventEmitter<string>();

  get buttonClass() { return this.modelHeli.picturePath ? 'ok' : 'danger'; }

  constructor(private client: InsBrandsClient, private mdserv: WindowService) {
    this.modelHeli = new InsModel();
    this.modelHeli.id = 0;
    this.modelHeli.insBrandId = this.brandId;
  }
  get saveDisabled() {
    return this.modelHeli.name == null ||
      this.modelHeli.name.length === 0 ||
      this.modelHeli.picturePath == null ||
      this.modelHeli.picturePath.length === 0 ||
      this.modelHeli.price == null ||
      this.modelHeli.price <= 0;
  }

  editModel() {
    this.isBusy = true;
    this.client.updateInsModel(this.modelHeli).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('update success');
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Сохранение модели квадрокоптера', error.status);
      this.ngOnError.emit('update error ' + error.status);
    });
  }
  createModel() {
    this.isBusy = true;
    this.modelHeli.id = 0;
    this.modelHeli.insBrandId = this.brandId;
    this.client.createInsModel(this.modelHeli).subscribe(x => {
      this.isBusy = false;
      this.modelHeli = new InsModel();
      this.modelHeli.id = 0;
      this.modelHeli.insBrandId = this.brandId;
      this.ngOnSuccess.emit('creation success');
    }, error => {
      this.isBusy = false;
      this.modelHeli = new InsModel();
      this.modelHeli.id = 0;
      this.modelHeli.insBrandId = this.brandId;
      this.mdserv.showServerError('Создание модели квадрокоптера', error.status);
      this.ngOnError.emit('creation error ' + error.status);
    });
  }
  deleteModel() {
    this.isBusy = true;
    this.client.deleteInsModel(this.modelHeli.id).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('deletion success');
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Удаление модели квадрокоптера', error.status);
      this.ngOnError.emit('deletion error ' + error.status);
    });
  }

  showWindow() {
    this.mdserv.showDeleteMessage('Удаление модели', 'Вы действительно хотите удалить модель: ' +
      this.modelHeli.name + '?').subscribe(x => {
        if (x) { this.deleteModel(); }
      });
  }

  showEditPhotoWindow() {
    this.mdserv.showUploadImageWindow(this.modelHeli.picturePath).subscribe(x => {
      if (x) { this.modelHeli.picturePath = x; }
    });
  }


}
