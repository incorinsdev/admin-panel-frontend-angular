import { Component, Input, EventEmitter, Output } from '@angular/core';
import { InsBrand } from '../../../services/api/models/insBrand.model';

@Component({
  selector: 'app-ins-model-controller',
  templateUrl: './insModel-controller.component.html'
})
export class InsModelControllerComponent {

  @Input() brand: InsBrand;

  get modelArray() { return this.brand ? this.brand.brandModels : []; }

  @Output() ngNeedUpdate: EventEmitter<string> = new EventEmitter<string>();

  updateOnSucces(event: string) {
    this.ngNeedUpdate.emit(event);
  }
  updateOnError(event: string) {
    this.ngNeedUpdate.emit(event);
  }

}
