import { Component } from '@angular/core';
import { InsBrand } from '../../../services/api/models/insBrand.model';
import { InsBrandsClient } from '../../../services/api/insBrand.service';
import { WindowService } from '../../../services/window.service';

@Component({
  selector: 'app-ins-brand-controller',
  templateUrl: './insBrand-controller.component.html',
  styleUrls: ['./insBrand-controller.component.scss']
})
export class InsBrandControllerComponent {

  brandArray: Array<InsBrand>;
  selectedBrand: InsBrand;
  constructor(private client: InsBrandsClient, private mdserv: WindowService) {
    this.brandArray = [];
    this.updateBrands();
  }


  updateBrands() {
    this.client.getInsBrands().subscribe(data => {
      this.brandArray = data.sort((a, b) => a.id > b.id ? 1 : -1);
      this.selectedBrand = this.selectedBrand ? this.brandArray.find(x => x.id === this.selectedBrand.id) : null;
    }, error => {
      this.mdserv.showServerError('Загрузка марок квадрокоптеров', error.status);
    });
  }

  updateOnSucces(event: string) { this.updateBrands(); }
  updateOnError(event: string) { this.updateBrands(); }
}
