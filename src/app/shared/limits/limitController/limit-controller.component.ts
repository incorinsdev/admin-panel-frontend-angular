import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Limit } from '../../../services/api/models/limit.model';

@Component({
  selector: 'app-limit-controller',
  templateUrl: './limit-controller.component.html',
  styleUrls: ['./limit-controller.component.scss']
})
export class LimitControllerComponent {
  @Input() limitArray: Array<Limit>;
  @Input() currency = 0;
  constructor() { this.limitArray = []; }

  @Output() hasToUpdate: EventEmitter<string> = new EventEmitter<string>();
  @Output() errorOccured: EventEmitter<string> = new EventEmitter<string>();

  updateOnSucces(event: string) { this.hasToUpdate.emit(event); }
  updateOnError(event: string) { this.errorOccured.emit(event); }

}
