import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CurrencyType } from '../../../services/api/models/currencyType.model';
import { Limit } from '../../../services/api/models/limit.model';
import { LimitsClient } from '../../../services/api/limits.service';
import { WindowService } from '../../../services/window.service';

@Component({
  selector: 'app-limit',
  templateUrl: './limit.component.html',
  styleUrls: ['./limit.component.scss']
})
export class LimitComponent {
  public isBusy = false;

  @Input() type = 'newLimit';
  @Input() get currency() { return this.limit.currency; }
  set currency(val: CurrencyType) { if (this.limit.currency !== val) { this.limit.currency = val; } }

  get saveDisabled() {
    return this.limit.awardLimit == null || this.limit.awardLimit <= 0 || this.limit.price == null || this.limit.price <= 0;
  }

  get icon() {
    if (this.currency === CurrencyType.Rub) {
      return 'fa-ruble-sign';
    } else if (this.currency === CurrencyType.Euro) {
      return 'fa-euro-sign';
    } else if (this.currency === CurrencyType.Dollar) {
      return 'fa-dollar-sign';
    }
  }
  @Input() limit: Limit;
  @Output() ngOnError: EventEmitter<string> = new EventEmitter<string>();
  @Output() ngOnSuccess: EventEmitter<string> = new EventEmitter<string>();

  constructor(private client: LimitsClient, private mdserv: WindowService) {
    this.limit = new Limit();
    this.limit.id = 0;
  }

  editLimit() {
    this.isBusy = true;
    this.client.updateLimit(this.limit).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('update success');
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Сохранение лимита страхования', error.status);
      this.ngOnSuccess.emit('update error ' + error.status);
    });
  }
  createLimit() {
    this.isBusy = true;
    this.client.createLimit(this.limit).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('creation success');
      this.limit = new Limit();
      this.limit.id = 0;
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Создание лимита страхования', error.status);
      this.ngOnSuccess.emit('creation error ' + error.status);
    });
  }
  deleteLimit() {
    this.isBusy = true;
    this.client.deleteLimit(this.limit.id).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('deletion success');
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Создание лимита страхования', error.status);
      this.ngOnSuccess.emit('deletion error ' + error.status);
    });
  }
  showWindow() {
    this.mdserv.showDeleteMessage('Удаление лимита', 'Вы действительно хотите удалить лимит: страховка на сумму '
      + this.limit.awardLimit + '₽ по цене ' + this.limit.price + '₽?').subscribe(x => {
        if (x) { this.deleteLimit(); }
      });
  }
}
