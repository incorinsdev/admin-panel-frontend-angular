import { Component, Input, Output, EventEmitter } from '@angular/core';
import { WindowService } from '../../../services/window.service';
import { Shop } from '../../../services/api/models/city-shop.model';
import { SellerShopClient } from '../../../services/api/sellerShop.service';
import { RepairShopClient } from '../../../services/api/repairShop.service';
import { IShopClient } from '../../../services/api/shop-service.interface';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent {

  public isBusy = false;
  @Input() type = 'newModel';
  @Input() brandId = 0;
  @Input() modelHeli: Shop;
  @Output() ngOnError: EventEmitter<string> = new EventEmitter<string>();
  @Output() ngOnSuccess: EventEmitter<string> = new EventEmitter<string>();

  get buttonClass() { return this.modelHeli.picUrl ? 'ok' : 'danger'; }

  private client: IShopClient;

  private _isSeller = true;
  @Input()
  get isSeller() { return this._isSeller; }
  set isSeller(v: boolean) {
    if (v !== this._isSeller) {
      this._isSeller = v;
      this.client = v ? this.sellerClient : this.repairClient;
    }
  }

  constructor(private sellerClient: SellerShopClient, private repairClient: RepairShopClient, private mdserv: WindowService) {
    this.client = this.sellerClient;
    this.modelHeli = new Shop();
    this.modelHeli.id = 0;
    this.modelHeli.cityId = this.brandId;
  }
  get saveDisabled() {
    return this.modelHeli.title == null ||
      this.modelHeli.title.length === 0 ||
      this.modelHeli.picUrl == null ||
      this.modelHeli.siteUrl == null ||
      this.modelHeli.siteUrl.length === 0 ||
      this.modelHeli.address == null ||
      this.modelHeli.address.length === 0 ||
      this.modelHeli.picUrl.length === 0;
  }

  editModel() {
    this.isBusy = true;
    this.client.updateShop(this.modelHeli).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('update success');
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Сохранение модели квадрокоптера', error.status);
      this.ngOnError.emit('update error ' + error.status);
    });
  }
  createModel() {
    this.isBusy = true;
    this.modelHeli.id = 0;
    this.modelHeli.cityId = this.brandId;
    this.client.createShop(this.modelHeli).subscribe(x => {
      this.isBusy = false;
      this.modelHeli = new Shop();
      this.modelHeli.id = 0;
      this.modelHeli.cityId = this.brandId;
      this.ngOnSuccess.emit('creation success');
    }, error => {
      this.isBusy = false;
      this.modelHeli = new Shop();
      this.modelHeli.id = 0;
      this.modelHeli.cityId = this.brandId;
      this.mdserv.showServerError('Создание модели квадрокоптера', error.status);
      this.ngOnError.emit('creation error ' + error.status);
    });
  }
  deleteModel() {
    this.isBusy = true;
    this.client.deleteShop(this.modelHeli.id).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('deletion success');
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Удаление модели квадрокоптера', error.status);
      this.ngOnError.emit('deletion error ' + error.status);
    });
  }

  showWindow() {
    this.mdserv.showDeleteMessage('Удаление модели', 'Вы действительно хотите удалить модель: ' +
      this.modelHeli.title + '?').subscribe(x => {
        if (x) { this.deleteModel(); }
      });
  }

  showEditPhotoWindow() {
    this.mdserv.showUploadImageWindow(this.modelHeli.picUrl, 'partner').subscribe(x => {
      if (x) { this.modelHeli.picUrl = x; }
    });
  }


}
