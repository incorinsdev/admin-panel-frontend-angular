import { Component, Input, OnInit } from '@angular/core';
import { WindowService } from '../../../services/window.service';
import { City } from '../../../services/api/models/city-shop.model';
import { SellerShopClient } from '../../../services/api/sellerShop.service';
import { RepairShopClient } from '../../../services/api/repairShop.service';
import { IShopClient } from '../../../services/api/shop-service.interface';

@Component({
  selector: 'app-city-controller',
  templateUrl: './city-controller.component.html',
  styleUrls: ['./city-controller.component.scss']
})
export class CityControllerComponent {


  private client: IShopClient;

  private _isSeller = true;
  @Input()
  get isSeller() { return this._isSeller; }
  set isSeller(v: boolean) {
    if (v !== this._isSeller) {
      this._isSeller = v;
      this.client = v ? this.sellerClient : this.repairClient;
    }
    this.updateBrands();
  }

  get title() { return this.isSeller ? 'Партнеры по продаже' : 'Партнеры по ремонту'; }

  brandArray: Array<City>;
  selectedBrand: City;
  constructor(private sellerClient: SellerShopClient, private repairClient: RepairShopClient, private mdserv: WindowService) {
    this.client = this.sellerClient;
    this.brandArray = [];
  }


  updateBrands() {
    this.client.getAllCities().subscribe(data => {
      this.brandArray = data.sort((a, b) => a.id > b.id ? 1 : -1);
      this.selectedBrand = this.selectedBrand ? this.brandArray.find(x => x.id === this.selectedBrand.id) : null;
    }, error => {
      this.mdserv.showServerError('Загрузка городов', error.status);
    });
  }

  updateOnSucces(event: string) { this.updateBrands(); }
  updateOnError(event: string) { this.updateBrands(); }
}
