import { Component, Input, EventEmitter, Output } from '@angular/core';
import { WindowService } from '../../../services/window.service';
import { City } from '../../../services/api/models/city-shop.model';
import { SellerShopClient } from '../../../services/api/sellerShop.service';
import { RepairShopClient } from '../../../services/api/repairShop.service';
import { IShopClient } from '../../../services/api/shop-service.interface';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent {

  private client: IShopClient;

  public isBusy = false;
  public mode: 'show' | 'edit' = 'show';

  @Input() type = 'newBrand';
  @Input() selected = false;
  @Input() brand: City;

  private _isSeller = true;
  @Input()
  get isSeller() { return this._isSeller; }
  set isSeller(v: boolean) {
    if (v !== this._isSeller) {
      this._isSeller = v;
      this.client = v ? this.sellerClient : this.repairClient;
    }
  }

  get editIcon() { return this.mode === 'show' ? 'fa-edit' : 'fa-save'; }
  get deleteIcon() { return this.mode === 'show' ? 'fa-trash' : 'fa-times'; }
  get saveDisabled() { return this.brand.name == null || this.brand.name.length === 0; }

  @Output() ngOnError: EventEmitter<string> = new EventEmitter<string>();
  @Output() ngOnSuccess: EventEmitter<string> = new EventEmitter<string>();
  @Output() ngOnBrandSelect: EventEmitter<City> = new EventEmitter<City>();

  constructor(private sellerClient: SellerShopClient, private repairClient: RepairShopClient, private mdserv: WindowService) {
    this.client = this.sellerClient;
    this.brand = new City();
    this.brand.id = 0;
  }

  selectCity() { this.ngOnBrandSelect.emit(this.brand); }

  pushFirstButton() {
    if (this.mode === 'show') {
      this.mode = 'edit';
    } else {
      this.editCity();
    }
  }
  pushSecondButton() {
    if (this.mode === 'show') {
      this.showWindow();
    } else {
      this.mode = 'show';
      this.ngOnSuccess.emit('no edditing');
    }
  }

  editCity() {
    this.isBusy = true;
    this.client.updateCity(this.brand).subscribe(x => {
      this.isBusy = false;
      this.mode = 'show';
      this.ngOnSuccess.emit('update success');
    }, error => {
      this.isBusy = false;
      this.mode = 'show';
      this.mdserv.showServerError('Сохранение города', error.status);
      this.ngOnError.emit('update error ' + error.status);
    });
  }
  createCity() {
    this.isBusy = true;
    this.client.createCity(this.brand).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('creation success');
      this.brand = new City();
      this.brand.id = 0;
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Создание города', error.status);
      this.ngOnError.emit('creation error ' + error.status);
    });
  }
  deleteCity() {
    this.isBusy = true;
    this.client.deleteCity(this.brand.id).subscribe(x => {
      this.isBusy = false;
      this.ngOnSuccess.emit('deletion success ' + this.brand.id);
    }, error => {
      this.isBusy = false;
      this.mdserv.showServerError('Удаление города', error.status);
      this.ngOnError.emit('deletion error ' + error.status);
    });
  }

  showWindow() {
    this.mdserv.showDeleteMessage('Удаление города', 'При удалении города уничтожаться также и все партнеры, относящиеся к нему.'
      + 'Вы действительно хотите город: ' + this.brand.name + '?').subscribe(x => {
        if (x) { this.deleteCity(); }
      });
  }

}
