import { Component, Input, EventEmitter, Output } from '@angular/core';
import { City } from '../../../services/api/models/city-shop.model';

@Component({
  selector: 'app-shop-controller',
  templateUrl: './shop-controller.component.html'
})
export class ShopControllerComponent {

  @Input() brand: City;

  @Input() isSeller = true;

  get modelArray() { return this.brand ? this.brand.shops : []; }

  @Output() ngNeedUpdate: EventEmitter<string> = new EventEmitter<string>();

  updateOnSucces(event: string) {
    this.ngNeedUpdate.emit(event);
  }
  updateOnError(event: string) {
    this.ngNeedUpdate.emit(event);
  }

}
