import { environment } from '../../environments/environment';

export class Config {
    public static API_BASE_URL = environment.apiUrl;
    public static UPLOAD_URL = '/api/UploadFiles';
}
