import { Injectable } from '@angular/core';
import { GMModelWindowService } from '../GodModeControls/ModalWindow/modal.service';
import { Config } from './web.config';


@Injectable()
export class WindowService {

    constructor(public mdservice: GMModelWindowService) { }

    showServerError(eventTitle: string, code: number) {
        this.mdservice.openConfirmWindow('ОШИБКА!', 'Произошла ошибка при событии: ' + eventTitle + '! Обратитесь в службу поддержки.');
    }

    showDeleteMessage(title, message) {
        return this.mdservice.openConfirmWindow(title, message, false).window.ngOnClose;
    }

    showUploadImageWindow(filePath: string, type: string = 'heli') {
        return this.mdservice.openUploadPhotoWindow(
            Config.API_BASE_URL, Config.UPLOAD_URL, filePath ? filePath : '', type).window.ngOnClose;
    }


}
