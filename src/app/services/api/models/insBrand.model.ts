import { InsModel } from './insModel.model';

export class InsBrand {
    id!: number;
    name!: string;
    brandModels?: InsModel[] | null;

    constructor(data?: InsBrand) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }
    static fromJS(data: any): InsBrand {
        data = typeof data === 'object' ? data : {};
        const result = new InsBrand();
        result.init(data);
        return result;
    }
    init(data?: any) {
        if (data) {
            this.id = data['id'] !== undefined ? data['id'] : <any>null;
            this.name = data['name'] !== undefined ? data['name'] : <any>null;
            if (data['brandModels'] && data['brandModels'].constructor === Array) {
                this.brandModels = [];
                for (const item of data['brandModels']) {
                    this.brandModels.push(InsModel.fromJS(item));
                }
            }
        }
    }
    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['id'] = this.id !== undefined ? this.id : <any>null;
        data['name'] = this.name !== undefined ? this.name : <any>null;
        if (this.brandModels && this.brandModels.constructor === Array) {
            data['brandModels'] = [];
            for (const item of this.brandModels) {
                data['brandModels'].push(item.toJSON());
            }
        }
        return data;
    }
}
