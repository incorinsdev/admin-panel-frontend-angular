export class InsModel {
    id!: number;
    name!: string;
    insBrandId!: number;
    price!: number;
    picturePath!: string;

    constructor(data?) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }
    static fromJS(data: any): InsModel {
        data = typeof data === 'object' ? data : {};
        const result = new InsModel();
        result.init(data);
        return result;
    }
    init(data?: any) {
        if (data) {
            this.id = data['id'] !== undefined ? data['id'] : <any>null;
            this.name = data['name'] !== undefined ? data['name'] : <any>null;
            this.insBrandId = data['insBrandId'] !== undefined ? data['insBrandId'] : <any>null;
            this.price = data['price'] !== undefined ? data['price'] : <any>null;
            this.picturePath = data['picturePath'] !== undefined ? data['picturePath'] : <any>null;
        }
    }
    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['id'] = this.id !== undefined ? this.id : <any>null;
        data['name'] = this.name !== undefined ? this.name : <any>null;
        data['insBrandId'] = this.insBrandId !== undefined ? this.insBrandId : <any>null;
        data['price'] = this.price !== undefined ? this.price : <any>null;
        data['picturePath'] = this.picturePath !== undefined ? this.picturePath : <any>null;
        return data;
    }
}
