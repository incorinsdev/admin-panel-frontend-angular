export enum CurrencyType {
    Rub = 0,
    Dollar = 1,
    Euro = 2,
}
