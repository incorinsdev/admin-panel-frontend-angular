import { CurrencyType } from './currencyType.model';

export class Limit {
    id!: number;
    awardLimit!: number;
    price!: number;
    currency!: CurrencyType;

    constructor(data?) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }
    static fromJS(data: any): Limit {
        data = typeof data === 'object' ? data : {};
        const result = new Limit();
        result.init(data);
        return result;
    }
    init(data?: any) {
        if (data) {
            this.id = data['id'] !== undefined ? data['id'] : <any>null;
            this.awardLimit = data['awardLimit'] !== undefined ? data['awardLimit'] : <any>null;
            this.price = data['price'] !== undefined ? data['price'] : <any>null;
            this.currency = data['currency'] !== undefined ? data['currency'] : <any>null;
        }
    }
    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['id'] = this.id !== undefined ? this.id : <any>null;
        data['awardLimit'] = this.awardLimit !== undefined ? this.awardLimit : <any>null;
        data['price'] = this.price !== undefined ? this.price : <any>null;
        data['currency'] = this.currency !== undefined ? this.currency : <any>null;
        return data;
    }
}
