export class ResModel {
    id!: number;
    name!: string;
    resBrandId!: number;

    constructor(data?) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }
    static fromJS(data: any): ResModel {
        data = typeof data === 'object' ? data : {};
        const result = new ResModel();
        result.init(data);
        return result;
    }
    init(data?: any) {
        if (data) {
            this.id = data['id'] !== undefined ? data['id'] : <any>null;
            this.name = data['name'] !== undefined ? data['name'] : <any>null;
            this.resBrandId = data['resBrandId'] !== undefined ? data['resBrandId'] : <any>null;
        }
    }
    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['id'] = this.id !== undefined ? this.id : <any>null;
        data['name'] = this.name !== undefined ? this.name : <any>null;
        data['resBrandId'] = this.resBrandId !== undefined ? this.resBrandId : <any>null;
        return data;
    }
}
