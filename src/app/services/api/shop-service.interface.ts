
import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { HttpResponseBase } from '@angular/common/http';
import { City, Shop } from './models/city-shop.model';

export interface IShopClient {
    getAllCities(): Observable<City[] | null>;
    createCity(city: City | null): Observable<void>;
    updateCity(city: City | null): Observable<void>;
    deleteCity(id: number): Observable<void>;

    createShop(shop: Shop | null): Observable<void>;
    updateShop(shop: Shop | null): Observable<void>;
    deleteShop(id: number): Observable<void>;
}
