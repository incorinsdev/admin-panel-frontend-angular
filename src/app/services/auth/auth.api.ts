import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { Config } from '../web.config';
import { AuthModel, TokenModel } from '../../models/auth.model';
import { blobToText, throwException } from '../api/models/swagger.functions';

@Injectable()
export class TokenClient {
    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(@Inject(HttpClient) http: HttpClient) {
        this.http = http;
        this.baseUrl = Config.API_BASE_URL;
    }

    login(auth: AuthModel | null): Observable<TokenModel | null> {
        let url_ = this.baseUrl + '/api/Token/Login';
        url_ = url_.replace(/[?&]$/, '');

        const content_ = JSON.stringify(auth);

        const options_: any = {
            body: content_,
            observe: 'response',
            responseType: 'blob',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            })
        };

        return this.http.request('post', url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processLogin(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processLogin(<any>response_);
                } catch (e) {
                    return <Observable<TokenModel | null>><any>_observableThrow(e);
                }
            } else {
                return <Observable<TokenModel | null>><any>_observableThrow(response_);
            }
        }));
    }
    protected processLogin(response: HttpResponseBase): Observable<TokenModel | null> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        const _headers: any = {};
        if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); } }
        if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('A server error occurred.', status, _responseText, _headers);
            }));
        } else if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                const resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = resultData200 ? TokenModel.fromJS(resultData200) : <any>null;
                return _observableOf(result200);
            }));
        } else if (status === 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('A server error occurred.', status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('An unexpected server error occurred.', status, _responseText, _headers);
            }));
        }
        return _observableOf<TokenModel | null>(<any>null);
    }
}
