import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

    constructor(private auth: AuthService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.auth.token != null) {
            const authReq = req.clone({
                headers: req.headers.set('Authorization', `Bearer ${this.auth.token}`)
            });
            return next.handle(authReq);
        }
        return next.handle(req);
    }
}
