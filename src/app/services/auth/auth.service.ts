import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TokenClient } from './auth.api';
import { throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { AuthModel } from '../../models/auth.model';

@Injectable()
export class AuthService {

    private _token = '';
    private _expiredDate: Date;

    public get token(): string {
        if (this._token) {
            return this._token;
        } else {
            const tok = localStorage.getItem('token');
            if (tok) { this._token = tok; }
            return tok;
        }
    }
    public set token(value: string) {
        this._token = value;
        localStorage.setItem('token', this._token);
    }
    public get expiredDate(): Date {
        if (this._expiredDate) {
            return this._expiredDate;
        } else {
            const expDate = new Date(Number(localStorage.getItem('expDate')));
            if (expDate) { this._expiredDate = expDate; }
            return expDate;
        }
    }
    public set expiredDate(value: Date) {
        this._expiredDate = value;
        localStorage.setItem('expDate', (+this._expiredDate).toString());
    }
    public get isAuthenticated(): boolean {
        return this.token && this.expiredDate && +this.expiredDate >= Date.now();
    }

    constructor(public router: Router, private authAPI: TokenClient) { }

    public login(username: string, password: string) {
        return this.authAPI.login(new AuthModel(username, password)).pipe(_observableMergeMap((data) => {
            this.token = data.token;
            this.expiredDate = this.convertStringDate(data.validTo);
            return _observableOf('Login is successfull.');
        })).pipe(_observableCatch((data: any) => _observableThrow('Login failed!')));
    }

    public logout() {
        this.token = '';
        this.expiredDate = null;
        this.router.navigate(['login']);
    }

    private convertStringDate(date: string) {
        const dateParts = date.split(' ')[0].split('/').map((x) => Number(x));
        return new Date(2000 + dateParts[2], dateParts[0] - 1, dateParts[1]);
    }
}
