export interface IAuthModel {
    username?: string | null;
    password?: string | null;
}

export class AuthModel implements IAuthModel {
    username?: string | null;
    password?: string | null;

    constructor(username: string, password: string) {
        this.username = username;
        this.password = password;
    }
}

export interface ITokenModel {
    token?: string | null;
    validTo?: string | null;
    user?: any | null;
    data?: any | null;
    userRole?: string | null;
}

export class TokenModel implements ITokenModel {
    token?: string | null;
    validTo?: string | null;
    user?: any | null;
    data?: any | null;
    userRole?: string | null;

    static fromJS(data: any): TokenModel {
        data = typeof data === 'object' ? data : {};
        const result = new TokenModel();
        result.init(data);
        return result;
    }

    constructor(data?: ITokenModel) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.token = data['token'] !== undefined ? data['token'] : <any>null;
            this.validTo = data['validTo'] !== undefined ? data['validTo'] : <any>null;
            this.user = data['user'] !== undefined ? data['user'] : <any>null;
            this.data = data['data'] !== undefined ? data['data'] : <any>null;
            this.userRole = data['userRole'] !== undefined ? data['userRole'] : <any>null;
        }
    }



    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['token'] = this.token !== undefined ? this.token : <any>null;
        data['validTo'] = this.validTo !== undefined ? this.validTo : <any>null;
        data['user'] = this.user !== undefined ? this.user : <any>null;
        data['data'] = this.data !== undefined ? this.data : <any>null;
        data['userRole'] = this.userRole !== undefined ? this.userRole : <any>null;
        return data;
    }
}



