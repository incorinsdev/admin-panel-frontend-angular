import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent {

  public username = '';
  public password = '';
  public hasError = false;
  public isBusy = false;

  constructor(public auth: AuthService, public router: Router) {
    if (this.auth.isAuthenticated) {
      this.router.navigate(['']);
    }
  }

  login() {
    this.isBusy = true;
    this.auth.login(this.username, this.password).subscribe(data => {
      this.hasError = false;
      this.isBusy = false;
      this.router.navigate(['']);
    }, error => {
      this.hasError = true;
      this.isBusy = false;
    });
  }

}
