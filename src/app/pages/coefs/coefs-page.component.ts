import { Component } from '@angular/core';
import { WindowService } from '../../services/window.service';
import { Limit } from '../../services/api/models/limit.model';
import { LimitsClient } from '../../services/api/limits.service';
import { CurrencyType } from '../../services/api/models/currencyType.model';

@Component({
  selector: 'app-coefs-page',
  templateUrl: './coefs-page.component.html',
  styleUrls: ['./coefs-page.component.scss']
})
export class CoefsPageComponent {

  RUBLimits: Array<Limit> = [];
  EURLimits: Array<Limit> = [];
  USDLimits: Array<Limit> = [];

  constructor(private client: LimitsClient, private mdserv: WindowService) {
    this.updateLimits();
  }

  prepareLimits(limits: Array<Limit>) {
    this.RUBLimits = [];
    this.EURLimits = [];
    this.USDLimits = [];
    limits.sort((a, b) => a.id > b.id ? 1 : -1).forEach(lim => {
      if (lim.currency === CurrencyType.Rub) {
        this.RUBLimits.push(lim);
      } else if (lim.currency === CurrencyType.Euro) {
        this.EURLimits.push(lim);
      } else if (lim.currency === CurrencyType.Dollar) {
        this.USDLimits.push(lim);
      }
    });
  }

  updateLimits() {
    this.client.getLimits().subscribe(limits => { this.prepareLimits(limits); }, error => {
      this.mdserv.showServerError('Загрузка лимитов страхования', error.status);
    });
  }



}
