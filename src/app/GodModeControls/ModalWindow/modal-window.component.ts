import { Component, ElementRef, EventEmitter, Output, ViewChild, OnInit } from '@angular/core';
import { GMModalWindowPosition } from './modal-window-position.model';

const REPOSITION_HEIGHT = 30;
const WINDOW_PADDING = 10;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class GMModalWindow implements OnInit {

  public closeResult: any;
  public position: GMModalWindowPosition;
  public windowClosed = false;
  public windowId: string;

  private canPosition = false;

  @ViewChild('window') windRef: ElementRef;
  verticalPosition: 'top' | 'bottom' | 'none' = 'none';

  @Output() ngPositionChange: EventEmitter<'top' | 'bottom' | 'none'> = new EventEmitter<'top' | 'bottom' | 'none'>();

  private get backDropHeight() { return document.querySelector('.gm-modalBackdrop').scrollHeight; }

  public get xPosition() {
    return (this.position && this.canPosition) ? (this.position.x - WINDOW_PADDING) + 'px' : '50%';
  }
  public get topPosition() {
    if (this.position && this.canPosition) {
      const height = document.getElementById(this.windowId).scrollHeight;
      const windowHeight = this.backDropHeight;
      if (height > 0 && this.position.ytop + height < windowHeight - REPOSITION_HEIGHT) {
        if (this.verticalPosition === 'bottom') {
          this.verticalPosition = 'top';
          this.ngPositionChange.emit(this.verticalPosition);
        }
        return (this.position.ytop - WINDOW_PADDING) + 'px';
      } else {
        return 'auto';
      }
    }
    return '50%';
  }
  public get bottomPosition() {
    if (this.position && this.canPosition) {
      const height = document.getElementById(this.windowId).scrollHeight;
      const windowHeight = this.backDropHeight;
      if (height === 0 || this.position.ytop + height < windowHeight - REPOSITION_HEIGHT) {
        return 'auto';
      } else {
        if (this.verticalPosition === 'top') {
          this.verticalPosition = 'bottom';
          this.ngPositionChange.emit(this.verticalPosition);
        }
        return (this.backDropHeight - this.position.ybottom - WINDOW_PADDING) + 'px';
      }
    }
    return 'auto';
  }
  public get widthPosition() {
    return (this.position && this.canPosition) ?
      (this.position.width > 0 ? (this.position.width + 2 * WINDOW_PADDING) + 'px' : 'auto') : 'auto';
  }

  @Output() ngOnClose: EventEmitter<any> = new EventEmitter<any>();
  ngOnInit(): void {
    this.verticalPosition = 'top';
    setTimeout(() => {
      this.canPosition = true;
    }, 1);
  }
  close() { this.windowClosed = true; this.ngOnClose.emit(this.closeResult); }
  clickOnBackdrop(event) { if (event.target === document.querySelector('.gm-modalBackdrop')) { this.close(); } }
}
