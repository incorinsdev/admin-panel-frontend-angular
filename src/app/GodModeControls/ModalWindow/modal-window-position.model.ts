export class GMModalWindowPosition {
    x: number;
    ytop: number;
    ybottom: number;
    width: number;
}
