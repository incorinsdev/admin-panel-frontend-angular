import { Injectable, Injector, ComponentFactoryResolver, ApplicationRef, EmbeddedViewRef, Type } from '@angular/core';
import { GMModalWindow } from './modal-window.component';
import { GMConfirmWindowComponent } from '../ConfirmWindow/confirm-window.component';
import { ModalWindowContent } from './modal-window-content.class';
import { GMUploadPhotoWindowComponent } from '../UploadPhotoWindow/upload-photo-window.component';
import { GMModalWindowPosition } from './modal-window-position.model';


@Injectable()
export class GMModelWindowService {
  constructor(private appRef: ApplicationRef, private injector: Injector, private componentFactoryResolver: ComponentFactoryResolver) { }

  openWindowsIds: Array<number> = [];

  openWindow(content: Type<ModalWindowContent>, position?: GMModalWindowPosition): any {
    const newId = this.pushNewId();
    const windowRef = this.componentFactoryResolver.resolveComponentFactory(GMModalWindow).create(this.injector);

    this.appRef.attachView(windowRef.hostView);
    const domWindowElem = (windowRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    domWindowElem.querySelector('.gm-modal-window').id = 'gm-window' + newId;
    document.body.appendChild(domWindowElem);
    windowRef.instance.windowId = 'gm-window' + newId;
    const contentRef = this.componentFactoryResolver.resolveComponentFactory(content).create(this.injector);
    this.appRef.attachView(contentRef.hostView);
    const domElem = (contentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    document.getElementById('gm-window' + newId).appendChild(domElem);
    contentRef.instance.window = windowRef.instance;
    if (position) { contentRef.instance.window.position = position; }
    windowRef.instance.ngOnClose.subscribe(data => {
      this.appRef.detachView(contentRef.hostView);
      this.appRef.detachView(windowRef.hostView);
      contentRef.destroy();
      windowRef.destroy();
      this.openWindowsIds = this.openWindowsIds.filter(x => x !== newId);
    });
    return contentRef.instance;
  }

  private pushNewId(): number {
    let result = 0;
    while (this.openWindowsIds.indexOf(result) > -1) {
      result++;
    }
    this.openWindowsIds.push(result);
    return result;
  }

  openConfirmWindow(title: string, message: string, hasNoButton: boolean = true) {
    const window = this.openWindow(GMConfirmWindowComponent);
    window.title = title;
    window.message = message;
    window.hasNoButton = hasNoButton;
    return window;
  }
  openUploadPhotoWindow(baseUrl: string, uploadLocalUrl: string, filePath: string = '', type: string = 'heli') {
    const window = this.openWindow(GMUploadPhotoWindowComponent);
    window.baseUrl = baseUrl;
    window.uploadLocalUrl = uploadLocalUrl;
    window.filePath = filePath;
    window.type = type;
    return window;
  }

}



