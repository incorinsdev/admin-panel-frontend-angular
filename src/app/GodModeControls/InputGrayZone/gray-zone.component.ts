import { Component, Input } from '@angular/core';

@Component({
  selector: 'gm-gray-zone',
  templateUrl: './gray-zone.component.html',
  styleUrls: ['./gray-zone.component.scss']
})
export class GMGrayZoneComponent {
  @Input() icon = '';
  @Input() text = '';
}
