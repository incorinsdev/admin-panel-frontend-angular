import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { ValueAccessorFunctions } from '../shared/shared.functions';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-text-field',
  templateUrl: './text-field.component.html',
  styleUrls: ['./text-field.component.scss'],
  providers: [{ provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => GMTextFieldComponent), multi: true }]
})
export class GMTextFieldComponent implements ControlValueAccessor {

  public value: string;
  public _disabled = false;
  public focused = false;

  @Input() tabInd = 1;
  @Input() placeholder = '';
  @Input() type = 'text';


  private onChangeCallback: (_: any) => void = ValueAccessorFunctions.noop;
  private onTouchedCallback: () => void = ValueAccessorFunctions.noop;


  writeValue(obj: string): void {
    if (obj !== this.value) {
      this.value = obj;
      this.onChangeCallback(this.value);
    }
  }
  registerOnChange(fn: any): void { this.onChangeCallback = fn; }
  registerOnTouched(fn: any): void { this.onTouchedCallback = fn; }
  setDisabledState?(isDisabled: boolean): void {
    if (isDisabled !== this._disabled) {
      this._disabled = isDisabled;
    }
  }



}
