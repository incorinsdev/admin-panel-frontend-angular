import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GMButtonComponent } from './Button/button.component';
import { GMInputCollectionComponent } from './InputCollection/input-collection.component';
import { GMGrayZoneComponent } from './InputGrayZone/gray-zone.component';
import { GMTextFieldComponent } from './TextField/text-field.component';
import { GMModelWindowService } from './ModalWindow/modal.service';
import { GMModalWindow } from './ModalWindow/modal-window.component';
import { GMConfirmWindowComponent } from './ConfirmWindow/confirm-window.component';
import { GMImageUploaderComponent } from './ImageUploader/image-uploader.component';
import { GMUploadPhotoWindowComponent } from './UploadPhotoWindow/upload-photo-window.component';

@NgModule({
    declarations: [
        GMButtonComponent,
        GMInputCollectionComponent,
        GMGrayZoneComponent,
        GMTextFieldComponent,
        GMModalWindow,
        GMConfirmWindowComponent,
        GMUploadPhotoWindowComponent,
        GMImageUploaderComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        OverlayModule,
        BrowserAnimationsModule,
    ],
    entryComponents: [GMModalWindow, GMConfirmWindowComponent, GMUploadPhotoWindowComponent],
    providers: [GMModelWindowService],
    exports: [
        GMButtonComponent,
        GMInputCollectionComponent,
        GMGrayZoneComponent,
        GMTextFieldComponent,
        GMImageUploaderComponent
    ]
})
export class GodModeControlsModule { }
