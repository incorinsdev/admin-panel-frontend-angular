import { Component } from '@angular/core';
import { ModalWindowContent } from '../ModalWindow/modal-window-content.class';
import { Config } from '../../services/web.config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-upload-photo-window',
  templateUrl: './upload-photo-window.component.html',
  styleUrls: ['./upload-photo-window.component.scss']
})
export class GMUploadPhotoWindowComponent extends ModalWindowContent {
  baseUrl = Config.API_BASE_URL;
  uploadLocalUrl = '/api/UploadFiles';
  type = 'heli';

  filePath = '';

  get text() {
    return 'Учтите, что для нормального отображения' + (this.type === 'heli' ?
      'квадрокоптеров на сайте требуется загружать прозрачные КВАДРАТНЫЕ фотографии размером 256х256 в формате ".png".' :
      'логотипов на сайте требуется загружать прозрачные картинки или на белом фоне размером не более 200х140 в формате ".png" или ".jpg". '
      + 'Размер самого логотипа внутри изображения не должен превышать 130х70 пикселей.');
  }

  closeYes() {
    this.window.closeResult = this.filePath;
    this.window.close();
  }
  closeNo() {
    this.window.closeResult = null;
    this.window.close();
  }

}
