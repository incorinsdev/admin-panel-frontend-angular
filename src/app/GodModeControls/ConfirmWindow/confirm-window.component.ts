import { Component } from '@angular/core';
import { ModalWindowContent } from '../ModalWindow/modal-window-content.class';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-confirm-window',
  templateUrl: './confirm-window.component.html',
  styleUrls: ['./confirm-window.component.scss']
})
export class GMConfirmWindowComponent extends ModalWindowContent {
  title: string;
  message: string;
  hasNoButton = true;
  closeYes() {
    this.window.closeResult = true;
    this.window.close();
  }
  closeNo() {
    this.window.closeResult = false;
    this.window.close();
  }
}
