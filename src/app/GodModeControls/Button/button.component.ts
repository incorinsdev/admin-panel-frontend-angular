import { Component, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class GMButtonComponent {
  @Input() isBusy = false;
  @Input() icon = '';
  @Input() text = '';
  @Input() tabInd = 1;
  public focused = false;
  private _disabled = false;

  @Output() action: EventEmitter<void> = new EventEmitter<void>();

  @HostListener('document:keyup.Enter', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (this.focused) { this.onAction(); }
  }

  @Input() get disabled() { return this._disabled || this.isBusy; }
  set disabled(val: boolean) { if (val !== this._disabled) { this._disabled = val; } }

  onAction() {
    if (!this.disabled) {
      this.action.emit();
    }
  }
}
