import { Component, ElementRef, HostListener } from '@angular/core';

@Component({
  selector: 'gm-input-collection',
  templateUrl: './input-collection.component.html',
  styleUrls: ['./input-collection.component.scss']
})
export class GMInputCollectionComponent {
  wholeElementIsFocused = false;
  constructor(private el: ElementRef) { }


  @HostListener('document:keyup.Tab', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    setTimeout(this.checkFocus(), 1);
  }
  @HostListener('document:click', ['$event']) onClickHandler(event: KeyboardEvent) {
    this.checkFocus();
  }

  checkFocus() {
    let result = false;
    for (var i = 0; i < this.el.nativeElement.children[0].children.length; i++) {
      if (this.el.nativeElement.children[0].children[i].firstChild.classList.contains('focused')) {
        result = true;
      }
    }
    this.wholeElementIsFocused = result;
  }
}
