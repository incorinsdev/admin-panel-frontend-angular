import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoefsPageComponent } from './pages/coefs/coefs-page.component';
import { LoginPageComponent } from './pages/login/login-page.component';
import { HeaderComponent } from './shared/header/header.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { GodModeControlsModule } from './GodModeControls/god-mode-controls.module';
import { AuthGuardService } from './services/auth/auth-guard.service';
import { AuthService } from './services/auth/auth.service';
import { TokenClient } from './services/auth/auth.api';

import { RequestInterceptor } from './services/auth/request.interceptor';

import { WindowService } from './services/window.service';

import { LimitsClient } from './services/api/limits.service';
import { ResBrandsClient } from './services/api/resBrand.service';
import { InsBrandsClient } from './services/api/insBrand.service';
import { SellerShopClient } from './services/api/sellerShop.service';
import { RepairShopClient } from './services/api/repairShop.service';

import { LimitControllerComponent } from './shared/limits/limitController/limit-controller.component';
import { LimitComponent } from './shared/limits/limit/limit.component';
import { ResBrandControllerComponent } from './shared/res/resBrandController/resBrand-controller.component';
import { ResBrandComponent } from './shared/res/resBrand/resBrand.component';
import { ResModelControllerComponent } from './shared/res/resModelController/resModel-controller.component';
import { ResModelComponent } from './shared/res/resModel/resModel.component';
import { InsBrandControllerComponent } from './shared/ins/insBrandController/insBrand-controller.component';
import { InsBrandComponent } from './shared/ins/insBrand/insBrand.component';
import { InsModelControllerComponent } from './shared/ins/insModelController/insModel-controller.component';
import { InsModelComponent } from './shared/ins/insModel/insModel.component';
import { CityControllerComponent } from './shared/city-shop/cityController/city-controller.component';
import { CityComponent } from './shared/city-shop/city/city.component';
import { ShopControllerComponent } from './shared/city-shop/shopController/shop-controller.component';
import { ShopComponent } from './shared/city-shop/shop/shop.component';



const appRoutes: Routes = [
  { path: '', component: CoefsPageComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginPageComponent },
  { path: '**', redirectTo: '' }
];


@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    CoefsPageComponent,
    HeaderComponent,
    LimitControllerComponent,
    LimitComponent,
    ResBrandControllerComponent,
    ResBrandComponent,
    ResModelControllerComponent,
    ResModelComponent,
    InsBrandControllerComponent,
    InsBrandComponent,
    InsModelControllerComponent,
    InsModelComponent,
    CityControllerComponent,
    CityComponent,
    ShopControllerComponent,
    ShopComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes, { useHash: false }),
    HttpClientModule,
    GodModeControlsModule
  ],
  providers: [
    AuthService,
    AuthGuardService,
    TokenClient,
    LimitsClient,
    ResBrandsClient,
    InsBrandsClient,
    SellerShopClient,
    RepairShopClient,
    WindowService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
